-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: mysql1002.mochahost.com
-- Generation Time: Dec 11, 2015 at 05:55 PM
-- Server version: 5.5.32-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dochal_heelox_photos`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE IF NOT EXISTS `albums` (
  `AlbumID` int(11) NOT NULL auto_increment,
  `UserID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`AlbumID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`AlbumID`, `UserID`, `Name`) VALUES
(0, 0, 'Heh');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `PhotoID` int(11) NOT NULL auto_increment,
  `AlbumID` int(11) NOT NULL,
  `Photo` longblob NOT NULL,
  `Caption` varchar(255) DEFAULT NULL,
  `Type` varchar(25) NOT NULL,
  `Size` varchar(25) NOT NULL,
  `Name` varchar(25) NOT NULL,
  PRIMARY KEY (`PhotoID`),
  KEY `fk_Album` (`AlbumID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;


--
-- Table structure for table `thumbnails`
--

CREATE TABLE IF NOT EXISTS `thumbnails` (
  `ThumbnailID` int(11) NOT NULL auto_increment,
  `PhotoID` int(11) NOT NULL,
  `Thumbnail` longblob NOT NULL,
  PRIMARY KEY (`ThumbnailID`),
  KEY `fk_Photo` (`PhotoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UserID` int(11) NOT NULL auto_increment,
  `Email` varchar(62) NOT NULL,
  `Password` varchar(32) NOT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `Email`, `Password`) VALUES
(0, 'dawid.ochal@heelox.com', 'bfae303d791a590cee70480bcb979090');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `fk_Album` FOREIGN KEY (`AlbumID`) REFERENCES `albums` (`AlbumID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `thumbnails`
--
ALTER TABLE `thumbnails`
  ADD CONSTRAINT `fk_Photo` FOREIGN KEY (`PhotoID`) REFERENCES `photos` (`PhotoID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
