<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SignIn
 *
 * @author Dawid
 */
class SignIn {
    
    
    private $_db;
    public $attempted = false;
    
    public function __construct(PDO $dbo) {
        $this->_db = $dbo;
    }
    
    public function onPost($data) {
        $this->attempted = true;
        $query = $this->_db->prepare("SELECT * FROM users WHERE Email = :username AND Password = :password LIMIT 1");
        $query->bindValue(":username", $data["username"], PDO::PARAM_STR);
        $query->bindValue(":password", md5($data["password"]), PDO::PARAM_STR);
        $query->execute();
        if($query->rowCount() > 0) {
            $result = $query->fetch();
            $_SESSION["USER_ID"] = $result["UserID"];
            header("Location: /Index");
        }
    }
    
    public function onGet($data) {
            if(isset($_SESSION["USER_ID"])) {
                unset($_SESSION["USER_ID"]);
            } 
    }
    
    
    
}
