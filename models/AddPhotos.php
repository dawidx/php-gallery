<?php


/**
 * Description of AddPhotos
 *
 * @author Dawid
 */
class AddPhotos implements IRestricted {
    
    private $_db;
    
    public function __construct($db) {
        $this->_db = $db;
        
        //Checking if album belogs to that person
        $query = $this->_db->prepare("SELECT * FROM albums WHERE UserID = :user_id LIMIT 1");
        $query->bindValue(":user_id", $_SESSION["USER_ID"], PDO::PARAM_INT);
        $query->execute();
        $user = $query->fetch();
        if($_SESSION["USER_ID"] != $user["UserID"]) {
            header("HTTP/1.1 403 Unauthorized");
            echo 'You are not allowed to add photos to this album';
            exit;
        }
    }
    
    public function getTitle() {
        return "Add Photos";
    }
    
    public function getHeader() {
        return "Add Photos";
    }
    
    public function onPost($data) {
        
    }
    
    public function onGet($data) {
        
    }
}
