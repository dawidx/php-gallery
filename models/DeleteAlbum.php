<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DeleteAlbum
 *
 * @author Dawid
 */
class DeleteAlbum implements IRestricted {
    
    private $_db, $_album;
    public $success = false;
    public $attempted = false;
    
    public function __construct(PDO $db) {
        $this->_db = $db;
        
        if(isset($_GET['albumId']) && $_GET['albumId'] != '') {
            $exists = $this->_db->prepare("SELECT * FROM albums WHERE AlbumID = :album_id LIMIT 1");
            $exists->bindValue(":album_id", $_GET['albumId'], PDO::PARAM_INT);
            $exists->execute();
            if($exists->rowCount() > 0) {
                $this->_album = $exists->fetch();
                if($this->_album["UserID"] != $_SESSION["USER_ID"]) {
                    header("forbidden");
                }
            } else {
                header("doesn't exists");
            }
        } else {
            header("Bad request");
        }
    }
    
    public function onPost($data) {
        
    }
    
    public function onGet($data) {
        if(isset($data['confirm'])) {
            $this->attempted = true;
            if($data["confirm"] == "yes") {
                $this->_deleteAlbum();
            } else {
                header("Location: /Index");
            }
        }
    }
    
    private function _deleteAlbum() {
        $album = $this->_db->prepare("DELETE FROM albums WHERE AlbumID = :album_id LIMIT 1");
        $album->bindValue(":album_id", $_GET['albumId'], PDO::PARAM_INT);
        if($album->execute()) {
            $this->success = true;
        }
    }
    
    public function getTitle() {
        return "Delete Album";
    }
    
    public function getHeader() {
        return "Delete Album";
    }
    
}
