<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NewAlbum
 *
 * @author Dawid
 */
class EditAlbum implements IRestricted {

    private $_db;
    public $attempted = false;
    private $_album;
    public $success = false;
    public function __construct($db) {
        $this->_db = $db;
        
        if (isset($_GET['albumId']) && $_GET['albumId'] != '') {
        $this->_getAlbum($_GET['albumId']);
        }
    }
    
    private function _getAlbum($albumId) {
        
            $query = $this->_db->prepare("SELECT * FROM albums WHERE AlbumID = :album_id LIMIT 1");
            $query->bindValue(":album_id", $_GET['albumId'], PDO::PARAM_INT);
            $query->execute();
            $this->_album = $query->fetch();
    }

    public function onPost($data) {
        $this->attempted = true;
        if (isset($_GET['albumId']) && $_GET['albumId'] != '') {
            $query = $this->_db->prepare("UPDATE albums SET Name = :name WHERE AlbumID = :album_id LIMIT 1");
            $query->bindValue(":name", $data["name"], PDO::PARAM_STR);
            $query->bindValue(":album_id", $_GET['albumId'], PDO::PARAM_INT);
            $query->bindValue(":name", $data["name"], PDO::PARAM_STR);
            if ($query->execute()) {
                $this->success = true;
                $this->_getAlbum($_GET['albumId']);
            }
        } else {
            $query = $this->_db->prepare("INSERT INTO albums (UserID, Name) VALUES(:user_id, :name)");
            $query->bindValue(":user_id", $_SESSION["USER_ID"], PDO::PARAM_INT);
            $query->bindValue(":name", $data["name"], PDO::PARAM_STR);
            if ($query->execute()) {
                $this->success = true;
                header("Location: /AddPhotos?albumId=" . $this->_db->lastInsertId());
            }
        }
    }

    public function onGet($data) {
        
    }

    public function getHeader() {
        return "Create New Album";
    }

    public function getTitle() {
        return " New Album :: Your Galleries :: Heelox Photos";
    }

    public function getAlbum($field) {
        return isset($this->_album[$field]) ? $this->_album[$field] : null;
    }

}
