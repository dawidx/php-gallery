<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SignUp
 *
 * @author Dawid
 */
class SignUp {
    
    private $_db;
    public $attempted = false;
    public $error_messages;
    
    public function __construct(PDO $db) {
        $this->_db = $db;
        $this->error_messages = array();
    }
    
    public function onPost($data) {
        $this->attempted = true;
        if($this->vEmail($data["email"]) && $this->vPassword($data["password"]) && $this->vPasswords($data['password'], $data["repassword"])) {
                  
        $query = $this->_db->prepare("INSERT INTO users(Email,Password) VALUES(:email, :password)");
        $query->bindValue(":email", $_POST['email'], PDO::PARAM_STR);
        $query->bindValue(":password", md5($_POST['password']), PDO::PARAM_STR);
        if($query->execute()) {
            $_SESSION['USER_ID'] = $this->_db->lastInsertId();
            header("Location: /Index");
        } 
        } else {
        }
    }
    
    public function vPasswords($password, $repassword) {
        if($password == $repassword) {
            return true;
        } else {
            $this->error_messages[] = "The passwords don't match.";
            return false;
        }
    }
    
    public function vEmail($email) {
        
        if (strlen($email) > 5 && strlen($email) <= 30 && preg_match("/^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i", $email)) {
            return true;
        } else {
            $this->error_messages[] = "Your e-mail doesn't appear valid.";
            return false;
        }
        
    }
    
    public function vPassword($password) {
        if(strlen($password) > 5) {
            return true;
        } else {
            $this->error_messages[] = "The password needs to be at least 5 characters long.";
            return false;
        }
    }
    
    public function onGet($data) {
        
    }
    
}
