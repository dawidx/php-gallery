<?php
class ViewAlbum implements IRestricted {
    private $_db;
    
    public $album;
    
    public function __construct(PDO $db) {
        $this->_db = $db;
        
    }
    
    public function onPost($data) {
        
    }
    
    public function getHeader() {
        return $this->album["Name"];
    }
    
    public function getTitle() {
        return $this->album["Name"] . " :: Your Galleries :: Heelox Photos";
    }
    
    public function onGet($data) {
        $query = $this->_db->prepare("SELECT * FROM albums WHERE AlbumID = :album_id LIMIT 1");
        $query->bindValue(":album_id", $data['albumId'], PDO::PARAM_INT);
        $query->execute();
        if($query->rowCount() > 0) {
            $this->album = $query->fetch();
            if($this->album["UserID"] != $_SESSION["USER_ID"]) {
                header("HTTP/1.1 403 Forbidden");
                echo 'You can\'t view this album.';
                exit;
            }
        } else {
            header("HTTP/1.1 404 Not Found");
            echo 'Album not found.';
            exit;
        }
    }
    
    public function getPhotos() {
        $query = $this->_db->prepare("SELECT photos.PhotoID, thumbnails.PhotoID, photos.AlbumID, photos.Caption, thumbnails.Thumbnail FROM photos, thumbnails WHERE photos.AlbumID = :album_id AND thumbnails.PhotoID = photos.PhotoID");
        $query->bindValue(":album_id", $this->album["AlbumID"]);
        $query->execute();
        return $query->fetchAll();
    }
    
}