<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index
 *
 * @author Dawid
 */
class Index implements IRestricted {
    
    private $_db;
    
    public function __construct($db) {
        $this->_db = $db;
    }
    
    public function onPost($data) {
        
    }
    
    public function onGet($data) {
        
    }
    
    public function getAlbums() {
        $query = $this->_db->prepare("SELECT * FROM albums WHERE UserID = :user_id");
        $query->bindValue(":user_id", $_SESSION["USER_ID"]);
        $query->execute();
        return $query->fetchAll();
    }
    
    public function getThumbnail($albumId) {
        $query = $this->_db->prepare("SELECT PhotoID, AlbumID FROM photos WHERE AlbumID = :album_id LIMIT 1");
        $query->bindValue(":album_id", $albumId);
        $query->execute();
        if($query->rowCount() > 0) {
            $result = $query->fetch();
            return "/ajax/loadThumbnail.php?photoId=".$result["PhotoID"];
        } else {
            return "dummy image";
        }
    }
    
    public function getTitle() {
        return "Your Gallery :: Heelox Photos";
    }
    
    public function getHeader() {
        return "Your Galleries";
    }
}
