<?php
include("common.inc.php");

interface IRestricted {}

(string) $page = isset($_GET['url']) ? $_GET['url'] : "Index";

include("models/".$page.".php");

$model = new $page($db);
if($model instanceof IRestricted && !isset($_SESSION["USER_ID"])) {
    header("Location: /SignIn");
}
if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST") {
    $model->onPost($_POST);
} else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET") {
    $model->onGet($_GET);
} 
//Rendering the page
include("views/".$page.".php");
?>
