<?php

include '../common.inc.php';

$photo = file_get_contents('php://input');
$filename = $_SERVER['HTTP_X_FILE_NAME'];
$filesize = $_SERVER['HTTP_X_FILE_SIZE'];
$filetype = $_SERVER['HTTP_X_FILE_TYPE'];
$album = $_SERVER['HTTP_ALBUM'];

$album_q = $db->prepare("SELECT AlbumID,UserID FROM albums WHERE AlbumID = :album AND UserID = :user");
$album_q->bindValue(":album", $album, PDO::PARAM_INT);
$album_q->bindValue(":user", $_SESSION["USER_ID"], PDO::PARAM_INT);
$album_q->execute();
if ($album_q->rowCount() == 0) {
    header("HTTP/1.1 403 Unauthorized");
    echo 'You are not allowed to add photos to this album';
    exit;
}
$db->beginTransaction();
try {
    $photo_q = $db->prepare("INSERT INTO photos(AlbumID,Photo,Type,Size,Name) VALUES(:album_id,:photo,:type,:size,:name)");
    $photo_q->bindValue(":album_id", $album, PDO::PARAM_INT);
    $photo_q->bindValue(":photo", $photo, PDO::PARAM_LOB);
    $photo_q->bindValue(":type", $filetype, PDO::PARAM_STR);
    $photo_q->bindValue(":size", $filesize, PDO::PARAM_STR);
    $photo_q->bindValue(":name", $filename, PDO::PARAM_STR);
    if ($photo_q->execute()) {
        
    }

    $im = imagecreatefromstring($photo);
    $width = imagesx($im);
    $height = imagesy($im);
    $rate = $width/300;
    $images_fin = ImageCreateTrueColor(300, $height/$rate);
    imagecopyresampled($images_fin, $im, 0, 0, 0, 0, 300, $height/$rate, $width, $height);

    ob_start();
    imagejpeg($images_fin, null, 90);
    $thumbnail = ob_get_clean();


    $thumbnail_q = $db->prepare("INSERT INTO thumbnails(PhotoID,Thumbnail) VALUES(:photo_id,:thumbnail)");
    $thumbnail_q->bindValue(":photo_id", $db->lastInsertId(), PDO::PARAM_INT);
    $thumbnail_q->bindValue(":thumbnail", $thumbnail, PDO::PARAM_LOB);
    if ($thumbnail_q->execute()) {
        
    }
    $db->commit();
    ImageDestroy($im);
    ImageDestroy($images_fin);
} catch (Exception $ex) {
    $db->rollBack();
}
