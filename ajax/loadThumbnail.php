<?php

include '../common.inc.php';

function checkIfAllowed(PDO $dbo, $photo_id, $user_id) {
    $query = $dbo->prepare("SELECT PhotoID, AlbumID FROM photos WHERE PhotoID = :photo_id");
    $query->bindValue(":photo_id", $photo_id);
    $query->execute();
    $result = $query->fetch();

    $album_q = $dbo->prepare("SELECT UserID, AlbumID FROM albums WHERE AlbumID = :album_id AND UserID = :user_id");
    $album_q->bindValue(":album_id", $result["AlbumID"], PDO::PARAM_INT);
    $album_q->bindValue(":user_id", $user_id, PDO::PARAM_INT);
    $album_q->execute();
    if ($album_q->rowCount() > 0) {
        return true;
    } else {
        return false;
    }
}

if (isset($_GET['photoId'])) {
    if (checkIfAllowed($db, $_GET["photoId"], $_SESSION["USER_ID"])) {
        $query = $db->prepare("SELECT * FROM thumbnails WHERE PhotoID = :photo_id LIMIT 1");
        $query->bindValue(":photo_id", $_GET["photoId"], PDO::PARAM_INT);
        $query->execute();
        $result = $query->fetch();
        header('Content-Type: image/jpeg');
        imagejpeg(imagecreatefromstring($result["Thumbnail"]), null, 100);
    } else {
        header("HTTP/1.1 403 Forbidden");
        echo 'You can\'t view this photo.';
        exit;
    }
} else {
    header("HTTP/1.1 400 Bad Request");
    echo 'Photo Id has not been provided.';
    exit;
}

