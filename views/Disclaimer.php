<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

        <title></title>

        <!-- Styles -->
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="css/prettyPhoto.css">
        <link rel="stylesheet" type="text/css" href="css/settings.css" media="screen"/>
        <link rel="stylesheet" href="css/color-scheme/orange.css">
        <link rel="stylesheet" href="css/style.css"/>
        <!-- Base JS -->
        <script src="js/jquery.min.js"></script>
        <script src="js/main.js"></script>
    </head>
    <body>

        <div class="main transparent">

            <?php
            include("_common/topbar.php");
            ?>

            <div class="b-content transparent">
                <div class="b-layout">
                    <div class="row">
                        <div class="row-item col-1_4">
                            
                        </div>
                        <div class="row-item col-2_4 b-promo">
                            <h2>Disclaimer</h2>
                            <p>This is a free website. You can use it to store and access your photos. There is no guarantee that your photos will not get lost/hacked. By using this software, you agreeing to this risk.</p>
                        </div>
                        <div class="row-item col-1_4">
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </body>
</html>