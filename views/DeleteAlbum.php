<?php include "_common/head.inc.php" ?>
<?php
    if ($model->attempted) {
        ?>
        <div class="row">
            <?php
            if($model->success) {
                ?>
            
            <div class="b-message message-success">
                Update successful.
            </div>
            <?php
            } else {
                ?>
            <div class="b-message message-error">
                Something went wrong. Try again.
            </div>
            <?php
            }
            ?>
        </div>
        <?php
    } else {
    ?>
<div class="b-promo">
    <a href="/DeleteAlbum?albumId=<?=$_GET['albumId'];?>&confirm=yes" class="btn big green">Yes</a>
    <a href="/DeleteAlbum?albumId=<?=$_GET['albumId'];?>&confirm=no" class="btn big red">No</a>
    <h3>Are you sure?</h3>
    <p>You can't reverse this operation. The data will be permanently lost.</p>
</div>
    <?php } 

include("_common/footer.inc.php");