<?php
include "_common/head.inc.php";
?>
    <div class="b-promo">
        <a href="/EditAlbum" class="btn big orange">New Album</a>
        <h3>Create New Album</h3>
        <p>Create a new album and add photos.</p>
    </div>
<div class="gap" style="height: 40px;">
			</div>
<div class="row">
<!--
    <ul class="b-filter-list">
        <li data-filter="*" class="active">All</li>
        <li data-filter=".webdesign">Web Design</li>
        <li data-filter=".photography">Photography</li>
        <li data-filter=".identity">Identity</li>
        <li data-filter=".tehnology">Tehnology</li>
    </ul>
    <div class="b-filter-select">
        <div class="filter-current">
            All categories
        </div>
        <ul>
            <li data-filter="*" class="active">All</li>
            <li data-filter=".webdesign">Web Design</li>
            <li data-filter=".photography">Photography</li>
            <li data-filter=".identity">Identity</li>
            <li data-filter=".tehnology">Tehnology</li>
        </ul>
    </div>
-->
    <div class="row port b-works">
        <?php
        foreach($model->getAlbums() as $album) {
       ?>
        <div class="row-item col-1_4">
            <div class="work">
                <a href="/ViewAlbum?albumId=<?=$album["AlbumID"]?>" class="work-image">
                    <img src="<?=$model->getThumbnail($album["AlbumID"]);?>" alt="">
                    <div class="link-overlay icon-chevron-right">
                    </div>
                </a>
                <a href="/ViewAlbum?albumId=<?=$album["AlbumID"]?>" class="work-name"><?=$album["Name"];?></a>
            </div>
        </div>
        <?php
        }
        ?>
    </div>
</div>
<?php
include "_common/footer.inc.php";
