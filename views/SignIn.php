<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

        <title>Gallery</title>

        <!-- Styles -->
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="css/prettyPhoto.css">
        <link rel="stylesheet" type="text/css" href="css/settings.css" media="screen"/>
        <link rel="stylesheet" href="css/color-scheme/orange.css">
        <link rel="stylesheet" href="css/style.css"/>
        <!-- Base JS -->
        <script src="js/jquery.min.js"></script>
        <script src="js/main.js"></script>
    </head>
    <body>

        <div class="main transparent">
            <?php
            include("_common/topbar.php");
            ?>

            <div class="b-content transparent">
                <div class="b-layout">
                    <div class="row">
                        <div class="row-item col-1_4">
                        </div>
                        <div class="row-item col-2_4 b-promo">

                            <h2>Sign In</h2>
                            <?php
                            if ($model->attempted) {
                                ?>
                                <div class="b-message message-error">
                                    Incorrect username or password
                                </div>
                                <?php
                            }
                            ?>
                            <form class="b-form" action="/SignIn" method="POST">
                                <div class="row">
                                    <div class="row-item col-1_4">
                                        E-mail:
                                    </div>
                                    <div class="row-item col-1_4 input-wrap">
                                        <input type="text" name="username" class="field-name"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="row-item col-1_4">
                                        Password: 
                                    </div>
                                    <div class="row-item col-1_4 input-wrap">
                                        <input type="password" name="password"/>
                                    </div>
                                </div>
                                <div class="row">
                                    
                                <div class="row-item col-1_4"><br/><br/>
                                <a href="/SignUp" class="btn small purple">Don't have an account? Sign Up!</a>
                                </div>
                                    <div class="row-item col-1_4"><br/>
                                    <input type="submit" class="btn big green" name="submit" value="Sign In"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row-item col-1_4"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>