<?php include "_common/head.inc.php" ?>
<form action="/EditAlbum?albumId=<?=$model->getAlbum("AlbumID");?>" method="post">
    <?php
    if ($model->attempted) {
        ?>
        <div class="row">
            <?php
            if($model->success) {
                ?>
            
            <div class="b-message message-success">
                Update successful.
            </div>
            <?php
            } else {
                ?>
            <div class="b-message message-error">
                Something went wrong. Try again.
            </div>
            <?php
            }
            ?>
        </div>
        <?php
    }
    ?>
    <div class="row">
        <div class="row-item col-1_4">
            Album Name:
        </div>
        <div class="row-item col-2_4">
            <div class="input-wrap">
                <input type="text" name="name" placeholder="name" value="<?=$model->getAlbum("Name");?>">
            </div>
        </div>
    </div>
    <div class="row">
        <input class="btn big orange" name="submit" value="Next >" type="submit">
    </div>
</form>
<?php include "_common/footer.inc.php"; ?>