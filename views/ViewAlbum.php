<?php include("_common/head.inc.php"); ?>
<div class="b-promo">
    <a href="/AddPhotos?albumId=<?= $model->album["AlbumID"]; ?>" class="btn big orange">Add Photos</a>
    <a href="/EditAlbum?albumId=<?= $model->album["AlbumID"]; ?>" class="btn big purple">Edit Album</a>
    <a href="/DeleteAlbum?albumId=<?= $model->album["AlbumID"]; ?>" class="btn big red">Delete Album</a>
    <h3>Manage Album</h3>
    <p> </p>
</div>
<div class="gap" style="height: 40px;">
</div>
<!-- Gallery -->
<div class="row">
    <?php
    foreach ($model->getPhotos() as $thumb) {
        ?>
    <div class="row-item col-1_4">
        <div class="img-wrap">
            <a class="pretty-photo-item" rel="prettyPhoto[gallery]" href="/ajax/loadPhoto.php?photoId=<?= $thumb["PhotoID"]; ?>">
                <img src="data:image/jpeg;base64, <?= base64_encode($thumb["Thumbnail"]); ?>" alt="">
                <div class="link-overlay icon-search"></div>
                <div class="img-title">
                    <?=$thumb["Caption"];?>
                </div>
            </a>
        </div>
        
    </div>
        <?php
    }
    ?>

</div>
<?php
include("_common/footer.inc.php");
