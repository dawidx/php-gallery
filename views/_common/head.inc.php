<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" b-content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

        <title><?=$model->getTitle();?></title>

        <!-- Styles -->
        <link rel="stylesheet" href="/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/css/main.css"/>
        <link rel="stylesheet" href="/css/responsive.css"/>
        <link rel="stylesheet" href="/css/prettyPhoto.css"/>
        <link rel="stylesheet" type="/text/css" href="css/settings.css" media="screen"/>
        <link rel="stylesheet" href="/css/color-scheme/orange.css"/>
        <link rel="stylesheet" href="/css/style.css"/>
        <link rel="stylesheet" href="/js/fuelux/fuelux.css"/>
        <!-- Base JS -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/main.js"></script>
        <script src="/js/bootstrap.js"></script>
        <script src="/js/fuelux/fuelux.js"></script>
        <!-- Prety photo -->
        <script src="/js/jquery.prettyPhoto.js"></script>
        <script>
            $(document).ready(function () {
                $("a[rel^='prettyPhoto']").prettyPhoto();
            });
        </script>
        <script src="/js/filereader.js"></script>
        <script src="/js/script.js"></script>
        
    </head>

    <body>

        <div class="main" style="min-height: 100%;">

            
            <?php
            include("topbar.php");
            ?>

            <!-- HEADER 
            ============================================= -->
            <div class="header">
                <div class="b-layout clearfix">
                    <div class="mob-b-layout wrap-left">
                        <!-- Logo -->
                        <a href="index.html" class="logo"><img src="img/logo.png" alt=""></a>
                        <!-- Mobile Navigation Button -->
                        <div class="btn-menu icon-reorder"></div>
                        <!-- Navigation -->
                        <ul class="menu">
                            <!-- Item 1 -->
                            <li>
                                <a href="/Index">Home</a>
                            </li>
                            
                            <!-- Item 2 -->
                            <li>
                                <a href="/SignIn">Sign Out</a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END HEADER 
            ============================================= -->

            <!-- TITLE BAR
            ============================================= -->
            <div class="b-titlebar">
                <div class="b-layout">
                    <!-- Bread Crumbs -->
                    <ul class="crumbs">
                        <li>You are here:</li>
                        <li><a href="/Index">Home</a></li>
                    </ul>
                    <!-- Title -->
                    <h1><?=$model->getHeader();?></h1>
                </div>
            </div>
            <!-- END TITLE BAR
            ============================================= -->

	<div class="b-content" style="min-height:50%">
            <div class="b-layout"style="min-height: 100%;">
                